#include <iostream>
#include <sys/time.h>

using namespace std;

// Start of profiler
//
class Profiler
{
private:
    long startTime;
    long endTime;

    long getCurTimeMs();

public:
    void start();
    void end();
    void printStats();
};

long Profiler::getCurTimeMs()
{
    struct timeval time;
    gettimeofday(&time, NULL);
    return time.tv_sec * 1000 + time.tv_usec;
}

void Profiler::start()
{
    this->startTime = this->getCurTimeMs();
}

void Profiler::end()
{
    this->endTime = this->getCurTimeMs();
}

void Profiler::printStats()
{
    cout << "Started at : " << this->startTime << "\n";
    cout << "Ended at : " << this->endTime << "\n";
    cout << "Elapsed : " << this->endTime - this->startTime << " ms\n";
}
//
// End of profiler

int maxSize = 0;
int *items;

void heapify(int root, int rHeapSize)
{
    int left = root * 2 + 1;
    int right = root * 2 + 2;

    int max = root;

    if (left < rHeapSize && items[left] > items[max])
    {
        max = left;
    }

    if (right < rHeapSize && items[right] > items[max])
    {
        max = right;
    }

    if (max != root)
    {
        int temp = items[max];
        items[max] = items[root];
        items[root] = temp;
        heapify(max, rHeapSize);
    }
}

void heapSort()
{
    for (int i = (maxSize / 2) - 1; i >= 0; i--)
    {
        heapify(i, maxSize);
    }

    for (int i = maxSize - 1; i > 0; --i)
    {
        auto temp = items[0];
        items[0] = items[i];
        items[i] = temp;
        heapify(0, i);
    }
}

void printArray()
{
    for (int i = 0; i < maxSize; ++i)
    {
        cout << items[i] << " ";
    }
    cout << endl;
}

int main()
{

    cout << "Enter The number of elements : ";
    cin >> maxSize;

    cout << "Enter the of elements to be sorted : ";
    items = new int[maxSize];
    for (int i = 0; i < maxSize; ++i)
    {
        cin >> items[i];
    }

    Profiler *profiler = new Profiler();
    profiler->start();

    heapSort();

    profiler->end();

    cout << "Sorted array : ";
    printArray();

    cout << "\nProfiler Information\n";
    profiler->printStats();

    free(profiler);
    free(items);

    return 0;
}