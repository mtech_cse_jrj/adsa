#include <iostream>
#include <list>
#include <fstream>
#include <sstream>

using namespace std;

ifstream MyReadFile("input.txt");

ofstream MyWriteFile("output.txt");

struct studentDetail
{
    int rollNo;
    string name;
};

struct Node
{
    int data, degree;
    studentDetail satellite;
    Node *child, *sibling, *parent;
};

class BinomialHeap
{

public:
    list<Node *> head;
    Node *makeHeap(int key, struct studentDetail satellite)
    {
        Node *temp = new Node;
        temp->data = key;
        temp->satellite = satellite;
        temp->degree = 0;
        temp->child = temp->parent = temp->sibling = NULL;
        return temp;
    }

    Node *binomialLink(Node *b1, Node *b2)
    {

        if (b1->data > b2->data)
        {
            swap(b1, b2);
        }
        b2->parent = b1;
        b2->sibling = b1->child;
        b1->child = b2;
        b1->degree++;

        return b1;
    }

    list<Node *> Union(list<Node *> &h1, list<Node *> &h2)
    {

        list<Node *> _new;
        list<Node *>::iterator it = h1.begin();
        list<Node *>::iterator ot = h2.begin();
        while (it != h1.end() && ot != h2.end())
        {

            if ((*it)->degree >= (*ot)->degree)
            {
                _new.push_back(*it);
                it++;
            }
            else
            {
                _new.push_back(*ot);
                ot++;
            }
        }

        while (it != h1.end())
        {
            _new.push_back(*it);
            it++;
        }

        while (ot != h2.end())
        {
            _new.push_back(*ot);
            ot++;
        }
        return _new;
    }

    void adjust()
    {
        if (head.size() >= 1)
            return;
        list<Node *> newheap;
        list<Node *>::iterator it1, it2, it3;
        it1 = it2 = it3 = head.begin();

        if (head.size() == 2)
        {
            it2 = it1;
            it2++;
            it3 = head.end();
        }
        else
        {
            it2++;
            it3 = it2;
            it3++;
        }
        while (it1 != head.end())
        {

            if (it2 == head.end())
                it1++;

            else if ((*it1)->degree > (*it2)->degree)
            {
                it1++;
                it2++;
                if (it3 != head.end())
                    it3++;
            }

            else if (it3 != head.end() &&
                     (*it1)->degree == (*it2)->degree &&
                     (*it1)->degree == (*it3)->degree)
            {
                it1++;
                it2++;
                it3++;
            }

            else if ((*it1)->degree == (*it2)->degree)
            {
                Node *temp;
                *it1 = binomialLink(*it1, *it2);
                it2 = head.erase(it2);
                if (it3 != head.end())
                    it3++;
            }
        }
    }

    void insertTree(Node *tree)
    {

        list<Node *> temp;

        temp.push_back(tree);

        head = Union(head, temp);

        adjust();
    }

    void insert(int key, struct studentDetail satellite)
    {
        Node *temp = makeHeap(key, satellite);
        insertTree(temp);
    }

    list<Node *> removeMinTree(Node *tree)
    {
        list<Node *> _heap;
        Node *temp = tree->child;
        Node *lo;

        while (temp)
        {
            lo = temp;
            temp = temp->sibling;
            lo->sibling = NULL;
            _heap.push_front(lo);
        }
        return _heap;
    }

    Node *findLargestNode()
    {
        list<Node *>::iterator it = head.begin();
        Node *temp = *it;
        while (it != head.end())
        {
            if ((*it)->data > temp->data)
                temp = *it;
            it++;
        }
        return temp;
    }

    void extractMax()
    {
        list<Node *> newheap, lo;
        Node *temp;

        temp = findLargestNode();
        list<Node *>::iterator it;
        it = head.begin();
        while (it != head.end())
        {
            if (*it != temp)
            {

                newheap.push_back(*it);
            }
            it++;
        }
        lo = removeMinTree(temp);
        head = Union(newheap, lo);
        adjust();
    }

    Node *findNode(Node *it, int val)
    {
        if (it == NULL)
            return NULL;

        if (it->data == val)
            return it;

        Node *res = findNode(it->child, val);
        if (res != NULL)
            return res;

        return findNode(it->sibling, val);
    }

    void decreaseKey(int old_val,
                     int new_val)
    {

        list<Node *>::iterator it = head.begin();
        Node *node = NULL;
        while (it != head.end() && node == NULL)
        {
            node = findNode(*it, old_val);
            it++;
        }

        if (node == NULL)
            return;

        node->data = new_val;
        Node *parent = node->parent;

        while (parent != NULL && node->data > parent->data)
        {
            swap(node->data, parent->data);
            node = parent;
            parent = parent->parent;
        }
    }

    void deleteNodeWithLargestKey()
    {
        auto node = this->findLargestNode();
        deleteNode(node->data);
    }

    void deleteNode(int val)
    {
        decreaseKey(val, 999999999);

        extractMax();
    }

    int curmax = 0;

    void scanTree(Node *h, int max)
    {
        while (h)
        {

            if (h->data > curmax && h->data < max)
            {
                curmax = h->data;
            }
            scanTree(h->child, max);
            h = h->sibling;
        }
    }

    Node *getNodeLessThan(int max)
    {
        curmax = 0;
        list<Node *>::iterator it;
        it = head.begin();
        while (it != head.end())
        {
            scanTree(*it, max);
            it++;
        }
        return find(curmax);
    }

    void printTree(Node *h)
    {
        while (h)
        {
            cout << "Mark: " << h->data << "\tRollNo.: " << h->satellite.rollNo << "\tName: " << h->satellite.name << "\n";
            MyWriteFile << "Mark: " << h->data << "\tRollNo.: " << h->satellite.rollNo << "\tName: " << h->satellite.name << "\n";
            printTree(h->child);
            h = h->sibling;
        }
    }

    void printHeap()
    {
        list<Node *>::iterator it;
        it = head.begin();
        while (it != head.end())
        {
            printTree(*it);
            it++;
        }
        cout << "\n";
    }

    Node *find(int key)
    {

        list<Node *>::iterator it = head.begin();
        Node *node = NULL;
        while (it != head.end() && node == NULL)
        {
            node = findNode(*it, key);
            it++;
        }

        return node;
    }
};

int main()
{

    int key, c;
    BinomialHeap heap;

    string myText;

    cout << "Reading heap\n";

    while (getline(MyReadFile, myText))
    {
        int rollNo, mark;
        char name[10];
        std::istringstream textStream(myText);
        textStream >> mark >> name >> rollNo;
        heap.insert(mark, {rollNo, name});
    }

    cout << "Heap is : \n\n";

    MyWriteFile << "Input Heap is \n";
    heap.printHeap();

    while (1)
    {
        cout << "1 Show first 5 students\n2 Delete Item\n3 find record\n4 Exit\n";
        cin >> c;
        switch (c)
        {
        case 1:
        {
            int i = 1;
            int lastMax = 999999999;
            MyWriteFile << "Student first 5 marks are \n";
            while (i <= 5)
            {

                Node *m1 = heap.getNodeLessThan(lastMax);
                heap.printTree(m1);
                lastMax = m1->data;
                ++i;
            }
            break;
        }
        case 2:
            cout << "Enter to mark to delete:";
            cin >> key;
            heap.deleteNode(key);
            cout << "Node Deleted.\n";
            break;
        case 3:
        {
            cout << "Enter to mark to find:";
            cin >> key;
            auto x = heap.find(key);
            MyWriteFile << "Student with Mark : " << key << " is \n";
            heap.printTree(x);
            break;
        }
        case 4:
            return 0;
        default:
            cout << "Invalid entry\n";
        }
    }

    return 0;
}
