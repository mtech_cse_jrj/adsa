#include <iostream>
#include <sys/time.h>

using namespace std;

// Start of profiler
//
class Profiler
{
private:
    long startTime;
    long endTime;

    long getCurTimeMs();

public:
    void start();
    void end();
    void printStats();
};

long Profiler::getCurTimeMs()
{
    struct timeval time;
    gettimeofday(&time, NULL);
    return time.tv_sec * 1000 + time.tv_usec;
}

void Profiler::start()
{
    this->startTime = this->getCurTimeMs();
}

void Profiler::end()
{
    this->endTime = this->getCurTimeMs();
}

void Profiler::printStats()
{
    cout << "Started at : " << this->startTime << "\n";
    cout << "Ended at : " << this->endTime << "\n";
    cout << "Elapsed : " << this->endTime - this->startTime << " ms\n";
}
//
// End of profiler

bool checkPrime(int n)
{

    if (n == 1)
    {
        return false;
    }
    else if (n == 2)
    {
        return true;
    }

    for (int i = 2; i < n / 2; i++)
    {
        if (n % i == 0)
        {
            return false;
        }
    }

    return true;
}

int main()
{

    int number;

    cout << "Enter The number to be tested : ";
    cin >> number;

    Profiler *primalityProflier = new Profiler();
    primalityProflier->start();
    bool isPrime = checkPrime(number);
    primalityProflier->end();

    cout << "\nThe number is " << (isPrime ? "" : "not ") << "Prime\n";

    cout << "\nProfiler Information\n";
    primalityProflier->printStats();

    free(primalityProflier);

    return 0;
}