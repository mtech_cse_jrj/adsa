#include <iostream>
#include <sys/time.h>

using namespace std;

// Start of profiler
//
class Profiler
{
private:
    long startTime;
    long endTime;

    long getCurTimeMs();

public:
    void start();

    void end();

    void printStats();
};

long Profiler::getCurTimeMs()
{
    struct timeval time;
    gettimeofday(&time, NULL);
    return time.tv_sec * 1000 + time.tv_usec;
}

void Profiler::start()
{
    this->startTime = this->getCurTimeMs();
}

void Profiler::end()
{
    this->endTime = this->getCurTimeMs();
}

void Profiler::printStats()
{
    cout << "Started at : " << this->startTime << "\n";
    cout << "Ended at : " << this->endTime << "\n";
    cout << "Elapsed : " << this->endTime - this->startTime << " ms\n";
}
//
// End of profiler

int noVertices = 0;
int startNode, endNode;
int **edges;

int *path, *distances;
bool *visited;

void readGraph()
{

    cout << "Enter The number of vertices : ";
    cin >> noVertices;

    cout << "Enter the start node : ";
    cin >> startNode;
    cout << "Enter the end node : ";
    cin >> endNode;

    edges = new int *[noVertices];
    path = new int[noVertices];
    distances = new int[noVertices];
    visited = new bool[noVertices];

    int ip;

    for (int i = 0; i < noVertices; i++)
    {
        edges[i] = new int[noVertices];
        for (int j = 0; j < noVertices; j++)
        {
            if (i == j)
            {
                edges[i][j] = 0;
                continue;
            }

            cout << "Enter The number weight of edge from node " << i << " to " << j << " (-1 for no edge) : ";
            cin >> ip;
            edges[i][j] = ip > 0 ? ip : INT32_MAX;
        }
    }
}

int getAdjacentNode()
{
    int node = 0;
    int minDistance = INT32_MAX;
    for (int i = 0; i < noVertices; i++)
    {
        if (visited[i])
        {
            continue;
        }
        if (distances[i] < minDistance)
        {
            minDistance = distances[i];
            node = i;
        }
    }
    return node;
}

void findPathDijkstra(int src = startNode)
{

    for (int i = 0; i < noVertices; ++i)
    {
        distances[i] = INT32_MAX;
    }

    distances[src] = 0;
    path[src] = -1;

    for (int i = 0; i < noVertices - 1; i++)
    {

        int anjNode = getAdjacentNode();
        visited[anjNode] = true;

        for (int j = 0; j < noVertices; j++)
        {

            if (visited[j])
            {
                continue;
            }

            int curDistance = edges[anjNode][j];
            int totalDistance = curDistance + distances[anjNode];
            if (totalDistance < distances[j] && curDistance != INT32_MAX)
            {
                path[j] = anjNode;
                distances[j] = totalDistance;
            }
        }
    }
}

void printOutput()
{
    cout << "Distance = " << distances[endNode] << "\nPath : ";
    int start = endNode;
    do
    {
        cout << start << " ";
        start = path[start];
    } while (start != -1);
}

int main()
{

    readGraph();

    Profiler *graphProfiler = new Profiler();
    graphProfiler->start();
    findPathDijkstra();
    
    graphProfiler->end();

    printOutput();
    
    free(edges);
    free(path);
    free(distances);
    free(visited);

    cout << "\nProfiler Information\n";
    graphProfiler->printStats();

    free(graphProfiler);

    return 0;
}