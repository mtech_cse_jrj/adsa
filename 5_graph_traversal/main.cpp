#include <iostream>
#include <sys/time.h>

using namespace std;

// Start of profiler
//
class Profiler
{
private:
    long startTime;
    long endTime;

    long getCurTimeMs();

public:
    void start();
    void end();
    void printStats();
};

long Profiler::getCurTimeMs()
{
    struct timeval time;
    gettimeofday(&time, NULL);
    return time.tv_sec * 1000 + time.tv_usec;
}

void Profiler::start()
{
    this->startTime = this->getCurTimeMs();
}

void Profiler::end()
{
    this->endTime = this->getCurTimeMs();
}

void Profiler::printStats()
{
    cout << "Started at : " << this->startTime << "\n";
    cout << "Ended at : " << this->endTime << "\n";
    cout << "Elapsed : " << this->endTime - this->startTime << " ms\n";
}
//
// End of profiler

int noVertices = 0;
bool **edges;
bool *visited;
int *stack;

void readGraph()
{

    noVertices = 4;

    cout << "Enter The number of vertices : ";
    cin >> noVertices;

    visited = new bool[noVertices];
    stack = new int[noVertices];
    edges = new bool *[noVertices];

    int ip = 0;

    for (int i = 0; i < noVertices; i++)
    {
        visited[i] = false;
        edges[i] = new bool[noVertices];
        for (int j = 0; j < noVertices; j++)
        {
            if (i == j)
            {
                edges[i][j] = true;
                continue;
            }

            cout << "Enter 1 if edge from " << i << " to " << j << " : ";
            cin >> ip;

            edges[i][j] = ip == 1;
        }
    }
}

bool isVisited(int node)
{
    return visited[node];
}

void dfs(int node)
{
    visited[node] = true;
    cout << node << " ";

    for (int i = 0; i < noVertices; i++)
    {
        if (edges[node][i] && (!isVisited(i)))
        {
            dfs(i);
        }
    }
}

int main()
{

    readGraph();

    Profiler *profiler = new Profiler();
    profiler->start();

    dfs(0);

    profiler->end();

    cout << "\nProfiler Information\n";
    profiler->printStats();

    free(profiler);
    free(edges);
    free(visited);
    free(stack);

    return 0;
}


