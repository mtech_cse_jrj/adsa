#include <iostream>
#include <cmath>
#include <string>
#include <fstream>
#include <sstream>
using namespace std;

struct studentDetail
{
    int rollNo;
    string name;
};

struct fNode
{
    studentDetail satellite;
    int value;
    int fDegree;
    fNode *fParent;
    fNode *fChild;
    fNode *fLeft;
    fNode *fRight;
    bool marked;
};
class fHeap
{
private:
    int nH;
    fNode *fH;

public:
    fNode *makeFHeap();
    fNode *createFNode(int);
    void insertNode(fNode *, struct studentDetail, int);
    fNode *deleteMaxKeyNode(fNode *);
    fNode *getMaxKeyNode();
    fNode *increaseKey(fNode *, int);
    fNode *decreaseKey(fNode *, int);
    int nodeCut(fNode *, fNode *, fNode *);
    int cascadeNodeCut(fNode *, fNode *);
    int Consolidate();
    int fibonacciLink(fNode *, fNode *);
    int Display(fNode *);
    fNode *findRecord(int, fNode *node);
    fHeap()
    {
        fH = makeFHeap();
    }
};
fHeap fh;
fNode *H = fh.makeFHeap();
fNode *fHeap::makeFHeap()
{
    fNode *p;
    p = NULL;
    return p;
}
fNode *fHeap::createFNode(int v)
{
    fNode *n = new fNode;
    n->value = v;
    return n;
}
void fHeap::insertNode(fNode *n, struct studentDetail satellite, int value)
{
    n->satellite = satellite;
    n->value = value;
    n->fDegree = 0;
    n->fParent = NULL;
    n->fChild = NULL;
    n->fLeft = n;
    n->fRight = n;
    n->marked = false;
    if (H != NULL)
    {
        (H->fLeft)->fRight = n;
        n->fRight = H;
        n->fLeft = H->fLeft;
        H->fLeft = n;
        if (n->value > H->value)
        {
            H = n;
        }
    }
    else
    {
        H = n;
    }
    nH = nH + 1;
}
fNode *fHeap::increaseKey(fNode *h, int l)
{
    if (!h)
    {
        return h;
    }
    h->value += l;
    fNode *p = h->fParent;
    if (p != NULL && h->value > p->value)
    {
        nodeCut(H, h, p);
        cascadeNodeCut(H, p);
    }
    if (h->value > H->value)
    {
        H = h;
    }
    return H;
}

fNode *fHeap::decreaseKey(fNode *h, int l)
{
    if (!h)
    {
        return h;
    }
    h->value -= l;
    fNode *p = h->fParent;
    if (p != NULL && h->value > p->value)
    {
        nodeCut(H, h, p);
        cascadeNodeCut(H, p);
    }
    if (h->value > H->value)
    {
        H = h;
    }
    return H;
}

fNode *fHeap::findRecord(int mark, fNode *node = nullptr)
{
    if (node == nullptr)
    {
        node = H;
    }

    fNode *h = node;
    int count = 0;

    while (h != nullptr)
    {
        count++;
        if (h->fChild != nullptr)
        {
            auto *fnode = this->findRecord(mark, h->fChild);
            if (fnode != nullptr)
            {
                return fnode;
            }
        }

        if (mark == h->value)
        {
            return h;
        }
        h = h->fRight;

        if (h == node)
        {
            return nullptr;
        }
    }

    return nullptr;
}

fNode *fHeap::getMaxKeyNode()
{

    auto max = H;
    auto h = H;

    while (h->fChild != nullptr)
    {
        h = h->fChild;
        cout << h->value;
        if (max->value < h->value)
        {
            max = h;
        }
    }

    return max;
}

fNode *fHeap::deleteMaxKeyNode(fNode *x)
{
    fNode **cList;
    fNode *p, *q, *next;
    p = H;
    if (p != NULL)
    {
        q = p->fChild;
        if (q != nullptr)
        {
            cList = new fNode *[p->fDegree];
            next = q;
            for (int j = 0; j < (int)p->fDegree; j++)
            {
                cList[j] = next;
                next = next->fRight;
            }
            for (int j = 0; j < (int)p->fDegree; j++)
            {
                q = cList[j];
                H->fLeft->fRight = q;
                q->fLeft = H->fLeft;
                H->fLeft = q;
                q->fRight = H;
                q->fParent = nullptr;
            }
        }
        p->fChild = nullptr;
        p->fDegree = 0;
        p->fLeft->fRight = p->fRight;
        p->fRight->fLeft = p->fLeft;
        if (p == p->fRight)
        {
            H = NULL;
        }
        else
        {
            H = p->fRight;
            Consolidate();
        }
        nH--;
    }
    return p;
}
int fHeap::Consolidate()
{
    fNode *w, *next, *x, *y, *temp;
    fNode **Array, **rList;
    int d, rSize;
    int max_degree = floor(log((double)(nH)) / log((double)(1 + sqrt((double)(5))) / 2));
    Array = new fNode *[max_degree + 2];
    fill_n(Array, max_degree + 2, nullptr);
    w = H;
    rSize = 0;
    next = w;
    do
    {
        rSize++;
        next = next->fRight;
    } while (next != w);
    rList = new fNode *[rSize];
    for (int i = 0; i < rSize; i++)
    {
        rList[i] = next;
        next = next->fRight;
    }
    for (int i = 0; i < rSize; i++)
    {
        w = rList[i];
        x = w;
        d = x->fDegree;
        while (Array[d] != nullptr)
        {
            y = Array[d];
            if (x->value < y->value)
            {
                temp = x;
                x = y;
                y = temp;
            }
            fibonacciLink(y, x);
            Array[d] = nullptr;
            d++;
        }
        Array[d] = x;
    }
    delete[] rList;
    H = nullptr;
    for (int i = 0; i < max_degree + 2; i++)
    {
        if (Array[i] != nullptr)
        {
            if (H == nullptr)
            {
                H = Array[i]->fLeft = Array[i]->fRight = Array[i];
            }
            else
            {
                H->fLeft->fRight = Array[i];
                Array[i]->fLeft = H->fLeft;
                H->fLeft = Array[i];
                Array[i]->fRight = H;
                if (Array[i]->value > H->value)
                {
                    H = Array[i];
                }
            }
        }
    }
    delete[] Array;
    return 0;
}
int fHeap::fibonacciLink(fNode *y, fNode *x)
{
    y->fLeft->fRight = y->fRight;
    y->fRight->fLeft = y->fLeft;
    if (x->fChild != nullptr)
    {
        x->fChild->fLeft->fRight = y;
        y->fLeft = x->fChild->fLeft;
        x->fChild->fLeft = y;
        y->fRight = x->fChild;
    }
    else
    {
        x->fChild = y;
        y->fRight = y;
        y->fLeft = y;
    }
    y->fParent = x;
    x->fDegree++;
    y->marked = false;
    return 0;
}
int fHeap::nodeCut(fNode *z, fNode *x, fNode *y)
{
    if (x == x->fRight)
    {
        y->fChild = NULL;
    }
    (x->fLeft)->fRight = x->fRight;
    (x->fRight)->fLeft = x->fLeft;
    if (x == y->fChild)
    {
        y->fChild = x->fRight;
    }
    y->fDegree = y->fDegree - 1;
    x->fRight = x;
    x->fLeft = x;
    (H->fLeft)->fRight = x;
    x->fRight = H;
    x->fLeft = H->fLeft;
    H->fLeft = x;
    x->fParent = NULL;
    x->marked = false;
    return 0;
}
int fHeap::cascadeNodeCut(fNode *t, fNode *y)
{
    fNode *z = y->fParent;
    if (z != NULL)
    {
        if (y->marked == false)
        {
            y->marked = true;
        }
        else
        {
            nodeCut(H, y, z);
            cascadeNodeCut(H, z);
        }
    }
    return 0;
}
using namespace std;
int main(int argc, char *argv[])
{

    fNode *p;

    cout << "Reading heap\n";

    ifstream MyReadFile("input.txt");

    string myText;
    while (getline(MyReadFile, myText))
    {
        int rollNo, mark;
        char name[10];
        std::istringstream textStream(myText);
        textStream >> mark >> name >> rollNo;
        p = fh.createFNode(mark);
        fh.insertNode(p, {rollNo, name}, mark);
    }

    int key;
    int c;

    while (1)
    {
        cout << "1 Show first 5 students\n2 Delete Item\n3 find record\n4 Exit\n";
        cin >> c;
        switch (c)
        {
        case 1:{

            cout << "Top 5 marks are:\n";
            int count = 5;
            while(--count >= 0){
                auto p = fh.deleteMaxKeyNode(H);
                cout << "mark : " << p->value << " name: " << p->satellite.name << " roll no: " << p->satellite.rollNo << "\n\n";
            }
            break;
        }
        case 2:
        {
            cout << "Enter to mark to delete:";
            cin >> key;
            p = fh.findRecord(key);
            if (p != nullptr)
            {
                fh.increaseKey(p, INT32_MAX);
                fh.deleteMaxKeyNode(H);
                cout << "Deleted\n\n";
            }
            else
            {
                cout << "Item Not found\n\n";
            }
            break;
        }
        case 3:
        {
            cout << "Enter to mark to find:";
            cin >> key;
            p = fh.findRecord(key);
            if (p != nullptr)
            {
                cout << "mark : " << p->value << " name: " << p->satellite.name << " roll no: " << p->satellite.rollNo << "\n\n";
            }
            else
            {
                cout << "Item Not found\n\n";
            }
            break;
        }
        case 4:
            return 0;
        default:
            cout << "Invalid entry\n";
        }
    }
    return 0;
    return 0;
}
