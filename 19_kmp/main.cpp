#include <iostream>
#include <string.h>
using namespace std;


void get_lps_array(char *pattern, int pat_len, int *lps)
{
    int len = 0;
    lps[0] = 0;
    int i = 1;
    while (i < pat_len)
    {
        if (pattern[i] == pattern[len])
            lps[i++] = ++len;
        else
        {
            if (len != 0)
                len = lps[len - 1];
            else
                lps[i++] = 0;
        }
    }
}

void kmp_search(char *pat, char *txt)
{
    int M = strlen(pat);
    int N = strlen(txt);

    int lps[M];

    get_lps_array(pat, M, lps);

    int i = 0;
    int j = 0;
    while (i < N)
    {
        if (pat[j] == txt[i])
        {
            j++;
            i++;
        }

        if (j == M)
        {
            cout << "Found pattern at index " <<  i - j << endl;
            j = lps[j - 1];
        }

        else if (i < N && pat[j] != txt[i])
        {

            if (j != 0)
                j = lps[j - 1];
            else
                i = i + 1;
        }
    }
}

int main()
{
    char txt[20000];
    char pat[200];

    cout << "Enter Text to search : ";
    cin >> txt;

    cout << "Enter Pattern : ";
    cin >> pat;

    kmp_search(pat, txt);
    return 0;
}
