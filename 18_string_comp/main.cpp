#include <bits/stdc++.h>
#include <iostream>
#include <sys/time.h>
using namespace std;

#define d 256

// Start of profiler
//
class Profiler
{
private:
    long startTime;
    long endTime;

    long getCurTimeMs();

public:
    void start();
    void end();
    void printStats();
};

long Profiler::getCurTimeMs()
{
    struct timeval time;
    gettimeofday(&time, NULL);
    return time.tv_sec * 1000 + time.tv_usec;
}

void Profiler::start()
{
    this->startTime = this->getCurTimeMs();
}

void Profiler::end()
{
    this->endTime = this->getCurTimeMs();
}

void Profiler::printStats()
{
    cout << "Started at : " << this->startTime << "\n";
    cout << "Ended at : " << this->endTime << "\n";
    cout << "Elapsed : " << this->endTime - this->startTime << " ms\n";
}
//
// End of profiler

void searchNaive(char *pat, char *txt)
{
    int str1Len = strlen(pat);
    int str2Len = strlen(txt);

    for (int i = 0; i <= str2Len - str1Len; i++)
    {
        int j;
        for (j = 0; j < str1Len; j++)
            if (txt[i + j] != pat[j])
                break;

        if (j == str1Len)
            cout << "Pattern found at "
                 << i << endl;
    }
}

void searchRabin(char pat[], char txt[])
{
    int q = 101;
    int str1Len = strlen(pat);
    int str2Len = strlen(txt);
    int i, j, p = 0, t = 0, h = 1;

    for (i = 0; i < str1Len - 1; i++)
        h = (h * d) % q;

    for (i = 0; i < str1Len; i++)
    {
        p = (d * p + pat[i]) % q;
        t = (d * t + txt[i]) % q;
    }

    for (i = 0; i <= str2Len - str1Len; i++)
    {
        if (p == t)
        {
            for (j = 0; j < str1Len; j++)
            {
                if (txt[i + j] != pat[j])
                    break;
            }

            if (j == str1Len)
                cout << "Pattern found at index " << i << endl;
        }

        if (i < str2Len - str1Len)
        {
            t = (d * (t - txt[i] * h) + txt[i + str1Len]) % q;
            if (t < 0)
                t = (t + q);
        }
    }
}

int main()
{
    char txt[20000];
    char pat[200];

    cout << "Enter Text to search : ";
    cin >> txt;

    cout << "Enter Pattern : ";
    cin >> pat;

    Profiler *matchProfiler = new Profiler();


    cout << "\nNaive Algorithm\n====================\n";

    matchProfiler->start();
    searchNaive(pat, txt);
    matchProfiler->end();
    cout << "\nProfiler Information\n";
    matchProfiler->printStats();


    cout << "\nRabin Karp Algorithm\n======================\n";

    matchProfiler->start();
    searchRabin(pat, txt);
    matchProfiler->end();
    cout << "\nProfiler Information\n";
    matchProfiler->printStats();

    free(matchProfiler);

    return 0;
}
