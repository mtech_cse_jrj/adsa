#include <iostream>
#include <cstring>
#include <cstdlib>
#include <stdio.h>
#include <algorithm>
#include <chrono>
#include <iostream>
#include <vector>
#include <gmp.h>
#include <gmpxx.h>
using namespace std;

mpz_class mulmod(mpz_class a, mpz_class b, mpz_class mod)
{
    mpz_class
        x = 0,
        y = a % mod;
    while (b > 0)
    {
        if (b % 2 == 1)
        {
            x = (x + y) % mod;
        }
        y = (y * 2) % mod;
        b /= 2;
    }
    return x % mod;
}

mpz_class modulo(mpz_class base, mpz_class exponent, mpz_class mod)
{
    mpz_class
        x = 1;
    mpz_class
        y = base;
    while (exponent > 0)
    {
        if (exponent % 2 == 1)
            x = mulmod(x, y, mod);
        y = mulmod(y, y, mod);
        exponent = exponent / 2;
    }
    return x % mod;
}

mpz_class oddFinder(mpz_class p)
{
    mpz_class t = 0, s = p - 1;
    while (s % 2 == 0)
    {
        s /= 2;
        t++;
    }
    cout << "n:" << p << " \nt:" << t << " \nu: " << s << "\n";
    return s;
}

bool MillerRabin(mpz_class p, int iteration)
{
    if (p < 2)
    {
        return false;
    }
    if (p != 2 && p % 2 == 0)
    {
        return false;
    }
    mpz_class s = oddFinder(p);
    for (int i = 0; i < iteration; i++)
    {
        mpz_class
            a = rand() % (p - 1) + 1,
            temp = s;
        mpz_class
            mod = modulo(a, temp, p);
        if (mod == 1 || mod == p - 1)
            continue;
        while (temp != p - 1 && mod != 1 && mod != p - 1)
        {
            mod = mulmod(mod, mod, p);
            temp *= 2;
        }
        if (mod != p - 1)
            return false;
    }
    return true;
}

int main()
{
    int tests = 5;
    mpz_class num;

    string numStr;

    cout << "Enter number :";
    cin >> numStr;

    num = numStr;

    if (MillerRabin(num, tests))
        cout << "Number is prime\n";
    else
        cout << "Number is not prime\n";
}