#include <iostream>
#include <cmath>
#include <string>
using namespace std;
struct fNode
{
    int value;
    int fDegree;
    fNode *fParent;
    fNode *fChild;
    fNode *fLeft;
    fNode *fRight;
    bool marked;
};
class fHeap
{
private:
    int nH;
    fNode *fH;

public:
    fNode *makeFHeap();
    fNode *createFNode(int);
    void insertNode(fNode *, int);
    fNode *deleteMaxKeyNode(fNode *);
    fNode *getMaxKeyNode();
    fNode *increaseKey(fNode *, int);
    fNode *decreaseKey(fNode *, int);
    int nodeCut(fNode *, fNode *, fNode *);
    int cascadeNodeCut(fNode *, fNode *);
    int Consolidate();
    int fibonacciLink(fNode *, fNode *);
    int Display(fNode *);
    fHeap()
    {
        fH = makeFHeap();
    }
};
fHeap fh;
fNode *H = fh.makeFHeap();
fNode *fHeap::makeFHeap()
{
    fNode *p;
    p = NULL;
    return p;
}
fNode *fHeap::createFNode(int v)
{
    fNode *n = new fNode;
    n->value = v;
    return n;
}
void fHeap::insertNode(fNode *n, int value)
{
    n->value = value;
    n->fDegree = 0;
    n->fParent = NULL;
    n->fChild = NULL;
    n->fLeft = n;
    n->fRight = n;
    n->marked = false;
    if (H != NULL)
    {
        (H->fLeft)->fRight = n;
        n->fRight = H;
        n->fLeft = H->fLeft;
        H->fLeft = n;
        if (n->value > H->value)
        {
            H = n;
        }
    }
    else
    {
        H = n;
    }
    nH = nH + 1;
}
fNode *fHeap::increaseKey(fNode *h, int l)
{
    if (!h)
    {
        return h;
    }
    h->value += l;
    fNode *p = h->fParent;
    if (p != NULL && h->value > p->value)
    {
        nodeCut(H, h, p);
        cascadeNodeCut(H, p);
    }
    if (h->value > H->value)
    {
        H = h;
    }
    return H;
}

fNode *fHeap::decreaseKey(fNode *h, int l)
{
    if (!h)
    {
        return h;
    }
    h->value -= l;
    fNode *p = h->fParent;
    if (p != NULL && h->value > p->value)
    {
        nodeCut(H, h, p);
        cascadeNodeCut(H, p);
    }
    if (h->value > H->value)
    {
        H = h;
    }
    return H;
}

fNode *fHeap::getMaxKeyNode()
{

    auto max = H;
    auto h = H;

    while (h->fChild != nullptr)
    {
        h = h->fChild;
        cout << h->value;
        if (max->value < h->value)
        {
            max = h;
        }
    }

    return max;
}

fNode *fHeap::deleteMaxKeyNode(fNode *x)
{
    fNode **cList;
    fNode *p, *q, *next;
    p = H;
    if (p != NULL)
    {
        q = p->fChild;
        if (q != nullptr)
        {
            cList = new fNode *[p->fDegree];
            next = q;
            for (int j = 0; j < (int)p->fDegree; j++)
            {
                cList[j] = next;
                next = next->fRight;
            }
            for (int j = 0; j < (int)p->fDegree; j++)
            {
                q = cList[j];
                H->fLeft->fRight = q;
                q->fLeft = H->fLeft;
                H->fLeft = q;
                q->fRight = H;
                q->fParent = nullptr;
            }
        }
        p->fChild = nullptr;
        p->fDegree = 0;
        p->fLeft->fRight = p->fRight;
        p->fRight->fLeft = p->fLeft;
        if (p == p->fRight)
        {
            H = NULL;
        }
        else
        {
            H = p->fRight;
            Consolidate();
        }
        nH--;
    }
    return p;
}
int fHeap::Consolidate()
{
    fNode *w, *next, *x, *y, *temp;
    fNode **Array, **rList;
    int d, rSize;
    int max_degree = floor(log((double)(nH)) / log((double)(1 + sqrt((double)(5))) / 2));
    Array = new fNode *[max_degree + 2];
    fill_n(Array, max_degree + 2, nullptr);
    w = H;
    rSize = 0;
    next = w;
    do
    {
        rSize++;
        next = next->fRight;
    } while (next != w);
    rList = new fNode *[rSize];
    for (int i = 0; i < rSize; i++)
    {
        rList[i] = next;
        next = next->fRight;
    }
    for (int i = 0; i < rSize; i++)
    {
        w = rList[i];
        x = w;
        d = x->fDegree;
        while (Array[d] != nullptr)
        {
            y = Array[d];
            if (x->value < y->value)
            {
                temp = x;
                x = y;
                y = temp;
            }
            fibonacciLink(y, x);
            Array[d] = nullptr;
            d++;
        }
        Array[d] = x;
    }
    delete[] rList;
    H = nullptr;
    for (int i = 0; i < max_degree + 2; i++)
    {
        if (Array[i] != nullptr)
        {
            if (H == nullptr)
            {
                H = Array[i]->fLeft = Array[i]->fRight = Array[i];
            }
            else
            {
                H->fLeft->fRight = Array[i];
                Array[i]->fLeft = H->fLeft;
                H->fLeft = Array[i];
                Array[i]->fRight = H;
                if (Array[i]->value > H->value)
                {
                    H = Array[i];
                }
            }
        }
    }
    delete[] Array;
    return 0;
}
int fHeap::fibonacciLink(fNode *y, fNode *x)
{
    y->fLeft->fRight = y->fRight;
    y->fRight->fLeft = y->fLeft;
    if (x->fChild != nullptr)
    {
        x->fChild->fLeft->fRight = y;
        y->fLeft = x->fChild->fLeft;
        x->fChild->fLeft = y;
        y->fRight = x->fChild;
    }
    else
    {
        x->fChild = y;
        y->fRight = y;
        y->fLeft = y;
    }
    y->fParent = x;
    x->fDegree++;
    y->marked = false;
    return 0;
}
int fHeap::nodeCut(fNode *z, fNode *x, fNode *y)
{
    if (x == x->fRight)
    {
        y->fChild = NULL;
    }
    (x->fLeft)->fRight = x->fRight;
    (x->fRight)->fLeft = x->fLeft;
    if (x == y->fChild)
    {
        y->fChild = x->fRight;
    }
    y->fDegree = y->fDegree - 1;
    x->fRight = x;
    x->fLeft = x;
    (H->fLeft)->fRight = x;
    x->fRight = H;
    x->fLeft = H->fLeft;
    H->fLeft = x;
    x->fParent = NULL;
    x->marked = false;
    return 0;
}
int fHeap::cascadeNodeCut(fNode *t, fNode *y)
{
    fNode *z = y->fParent;
    if (z != NULL)
    {
        if (y->marked == false)
        {
            y->marked = true;
        }
        else
        {
            nodeCut(H, y, z);
            cascadeNodeCut(H, z);
        }
    }
    return 0;
}
using namespace std;
int main(int argc, char *argv[])
{

    fNode *p;
    int key;
    int c;

    while (1)
    {
        cout << "1 Insert Item\n2 Find largest node\n3 Remove heighest Key node\n4 Decrease Key\n5 Exit\n";
        cin >> c;
        switch (c)
        {
        case 1:
            cout << "Enter to item to insert:";
            cin >> key;
            p = fh.createFNode(key);
            fh.insertNode(p, key);
            break;
        case 2:
        {
            auto n = fh.getMaxKeyNode();
            cout << "Largest node is :" << n->value << "\n";
            break;
        }
        case 3:
        {
            auto n = fh.deleteMaxKeyNode(H);
            cout << "Max key node : " << n->value << " deleted";
            break;
        }
        case 4:
        {
            int decreaseTo;
            cout << "Enter to decrease to value:";
            cin >> decreaseTo;
            fh.decreaseKey(fh.getMaxKeyNode(), decreaseTo);
            break;
        }
        case 5:
            return 0;
        default:
            cout << "Invalid entry\n";
        }
    }
    return 0;
    return 0;
}
