#include <array>
#include <iterator>
#include <cstdint>
#include <iostream>
#include <sys/time.h>

using namespace std;

// Start of profiler
//
class Profiler
{
private:
    long startTime;
    long endTime;

    long getCurTimeMs();

public:
    void start();
    void end();
    void printStats();
};

long Profiler::getCurTimeMs()
{
    struct timeval time;
    gettimeofday(&time, NULL);
    return time.tv_sec * 1000 + time.tv_usec;
}

void Profiler::start()
{
    this->startTime = this->getCurTimeMs();
}

void Profiler::end()
{
    this->endTime = this->getCurTimeMs();
}

void Profiler::printStats()
{
    cout << "Started at : " << this->startTime << "\n";
    cout << "Ended at : " << this->endTime << "\n";
    cout << "Elapsed : " << this->endTime - this->startTime << " ms\n";
}
//
// End of profiler

class md5
{
private:
    unsigned int A0 = 0x67452301;
    unsigned int B0 = 0xefcdab89;
    unsigned int C0 = 0x98badcfe;
    unsigned int D0 = 0x10325476;

    array<unsigned int, 16> ipArray;
    array<unsigned int, 16>::iterator ipArrayItr;

    static const array<unsigned int, 64> kArray;
    static const array<unsigned int, 64> sArray;

private:
    static unsigned int left_rotate(unsigned int x, unsigned int c)
    {
        return (x << c) | (x >> (32 - c));
    }

    template <class OutputIterator>
    static void uint32_to_byte(unsigned int n, OutputIterator &first)
    {

        *first++ = n & 0xff;
        *first++ = (n >> 8) & 0xff;
        *first++ = (n >> 16) & 0xff;
        *first++ = (n >> 24) & 0xff;
    }

    template <class OutputIterator>
    static void uint32_to_hex(unsigned int n, OutputIterator &first)
    {
        const char *hex_chars = "0123456789abcdef";

        unsigned int b;

        b = n & 0xff;
        *first++ = hex_chars[b >> 4];
        *first++ = hex_chars[b & 0xf];

        b = (n >> 8) & 0xff;
        *first++ = hex_chars[b >> 4];
        *first++ = hex_chars[b & 0xf];

        b = (n >> 16) & 0xff;
        *first++ = hex_chars[b >> 4];
        *first++ = hex_chars[b & 0xf];

        b = (n >> 24) & 0xff;
        *first++ = hex_chars[b >> 4];
        *first++ = hex_chars[b & 0xf];
    }

private:
    void reset_m_array()
    {
        ipArrayItr = ipArray.begin();
    }

    template <class InputIterator>
    void bytes_to_m_array(InputIterator &first, array<unsigned int, 16>::iterator m_array_last)
    {
        for (; ipArrayItr != m_array_last; ++ipArrayItr)
        {
            *ipArrayItr = *first++;
            *ipArrayItr |= *first++ << 8;
            *ipArrayItr |= *first++ << 16;
            *ipArrayItr |= *first++ << 24;
        }
    }

    template <class InputIterator>
    void true_bit_to_m_array(InputIterator &first, ptrdiff_t chunk_length)
    {
        switch (chunk_length % 4)
        {
        case 0:
            *ipArrayItr = 0x00000080;
            break;
        case 1:
            *ipArrayItr = *first++;
            *ipArrayItr |= 0x00008000;
            break;
        case 2:
            *ipArrayItr = *first++;
            *ipArrayItr |= *first++ << 8;
            *ipArrayItr |= 0x00800000;
            break;
        case 3:
            *ipArrayItr = *first++;
            *ipArrayItr |= *first++ << 8;
            *ipArrayItr |= *first++ << 16;
            *ipArrayItr |= 0x80000000;
            break;
        }
        ++ipArrayItr;
    }

    void zeros_to_m_array(array<unsigned int, 16>::iterator m_array_last)
    {
        for (; ipArrayItr != m_array_last; ++ipArrayItr)
        {
            *ipArrayItr = 0;
        }
    }

    void original_length_bits_to_m_array(uint64_t original_length_bits)
    {
        original_length_bits &= 0xffffffffffffffff;
        *ipArrayItr++ = (original_length_bits)&0x00000000ffffffff;
        *ipArrayItr++ = (original_length_bits & 0xffffffff00000000) >> 32;
    }

    void hash_chunk()
    {
        unsigned int A = A0;
        unsigned int B = B0;
        unsigned int C = C0;
        unsigned int D = D0;

        unsigned int F;
        unsigned int g;

        for (unsigned int i = 0; i < 64; ++i)
        {
            if (i < 16)
            {
                F = (B & C) | ((~B) & D);
                g = i;
            }
            else if (i < 32)
            {
                F = (D & B) | ((~D) & C);
                g = (5 * i + 1) & 0xf;
            }
            else if (i < 48)
            {
                F = B ^ C ^ D;
                g = (3 * i + 5) & 0xf;
            }
            else
            {
                F = C ^ (B | (~D));
                g = (7 * i) & 0xf;
            }

            unsigned int D_temp = D;
            D = C;
            C = B;
            B += left_rotate(A + F + kArray[i] + ipArray[g], sArray[i]);
            A = D_temp;
        }

        A0 += A;
        B0 += B;
        C0 += C;
        D0 += D;
    }

public:
    template <class InputIterator>
    void update(InputIterator first, InputIterator last)
    {

        uint64_t original_length_bits = distance(first, last) * 8;

        ptrdiff_t chunk_length;
        while ((chunk_length = distance(first, last)) >= 64)
        {
            reset_m_array();
            bytes_to_m_array(first, ipArray.end());
            hash_chunk();
        }

        reset_m_array();
        bytes_to_m_array(first, ipArray.begin() + chunk_length / 4);
        true_bit_to_m_array(first, chunk_length);

        if (chunk_length >= 56)
        {
            zeros_to_m_array(ipArray.end());
            hash_chunk();

            reset_m_array();
            zeros_to_m_array(ipArray.end() - 2);
            original_length_bits_to_m_array(original_length_bits);
            hash_chunk();
        }
        else
        {
            zeros_to_m_array(ipArray.end() - 2);
            original_length_bits_to_m_array(original_length_bits);
            hash_chunk();
        }
    }

public:
    template <class Container>
    void digest(Container &container)
    {
        container.resize(16);
        auto it = container.begin();

        uint32_to_byte(A0, it);
        uint32_to_byte(B0, it);
        uint32_to_byte(C0, it);
        uint32_to_byte(D0, it);
    }

    template <class Container>
    void hex_digest(Container &container)
    {
        container.resize(32);
        auto it = container.begin();

        uint32_to_hex(A0, it);
        uint32_to_hex(B0, it);
        uint32_to_hex(C0, it);
        uint32_to_hex(D0, it);
    }
};

const array<unsigned int, 64> md5::kArray = {
    0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
    0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
    0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
    0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
    0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
    0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
    0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
    0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
    0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
    0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
    0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
    0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
    0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
    0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
    0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
    0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391};

const array<unsigned int, 64> md5::sArray = {
    7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22,
    5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20,
    4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23,
    6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21};

int main()
{
    string data;
    cout << "Enter The string to be hashed : ";
    cin >> data;

    string dataHexDigest;

    Profiler *md5Profiler = new Profiler();
    md5Profiler->start();
    md5 hash;
    hash.update(data.begin(), data.end());
    hash.hex_digest(dataHexDigest);

    md5Profiler->end();
    cout << "The hash is : " << dataHexDigest << endl << endl;
    md5Profiler->printStats();

    return 0;
}