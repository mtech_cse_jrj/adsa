#include <iostream>
using namespace std;

struct node
{
    int priorityMark;
    int rollNo;
    string name;
    struct node *link;
};

class PriorityQueue
{
private:
    node *head;

public:
    PriorityQueue()
    {
        head = NULL;
    }
    void insert(int rollNo, string name, int priority)
    {
        node *tmp, *q;
        tmp = new node;
        tmp->rollNo = rollNo;
        tmp->name = name;
        tmp->priorityMark = priority;
        if (head == NULL || priority > head->priorityMark)
        {
            tmp->link = head;
            head = tmp;
        }
        else
        {
            q = head;
            while (q->link != NULL && q->link->priorityMark >= priority)
            {
                q = q->link;
            }
            tmp->link = q->link;
            q->link = tmp;
        }
    }

    void deleteItem()
    {
        if (head == NULL)
        {
            cout << "Queue Empty\n";
            return;
        }
        node *tmp;
        tmp = head;
        cout << "Deleted : " << tmp->rollNo << endl;
        head = head->link;
        free(tmp);
    }

    void printQueue()
    {
        node *ptr;
        ptr = head;
        cout << "Queue is :\n";
        while (ptr != NULL)
        {
            cout << ptr->priorityMark << "\t Name: " << ptr->name << "\t Roll No.:" << ptr->rollNo << endl;
            ptr = ptr->link;
        }
    }
};

int main()
{
    int c, rollNo, mark;
    string name;
    // char name[10];
    PriorityQueue pq;
    while (1)
    {
        cout << "1 Insert Item\n2 Delete Item\n3 Print Queue\n4 Exit\n";
        cin >> c;
        switch (c)
        {
        case 1:
            cout << "Enter the roll number : ";
            cin >> rollNo;
            cout << "Enter the name of the student : ";
            cin >> name;
            cout << "Enter the mark : ";
            cin >> mark;
            pq.insert(rollNo, name, mark);
            break;
        case 2:
            pq.deleteItem();
            break;
        case 3:
            pq.printQueue();
            break;
        case 4:
            return 0;
        default:
            cout << "Invalid entry\n";
        }
    }
    return 0;
}