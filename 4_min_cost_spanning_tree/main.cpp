#include <iostream>
#include <sys/time.h>

using namespace std;

// Start of profiler
//
class Profiler
{
private:
    long startTime;
    long endTime;

    long getCurTimeMs();

public:
    void start();
    void end();
    void printStats();
};

long Profiler::getCurTimeMs()
{
    struct timeval time;
    gettimeofday(&time, NULL);
    return time.tv_sec * 1000 + time.tv_usec;
}

void Profiler::start()
{
    this->startTime = this->getCurTimeMs();
}

void Profiler::end()
{
    this->endTime = this->getCurTimeMs();
}

void Profiler::printStats()
{
    cout << "Started at : " << this->startTime << "\n";
    cout << "Ended at : " << this->endTime << "\n";
    cout << "Elapsed : " << this->endTime - this->startTime << " ms\n";
}
//
// End of profiler

struct t_edge
{
    int source;
    int dest;
    int weight;
};
typedef t_edge Edge;
Edge *treeEdges;
int *parent;

int noEdges = 0;
Edge *edges;
int noVertex;

void readGraph()
{
    cout << "Enter The number of edges : ";
    cin >> noEdges;

    cout << "Enter The number of vertices : ";
    cin >> noVertex;

    cout << "Enter the vertices : (source, destination, weight)\n";

    edges = new Edge[noEdges];

    for (int i = 0; i < noEdges; i++)
    {
        cin >> edges[i].source >> edges[i].dest >> edges[i].weight;
    }
}

void printGraph()
{
    cout << "Selected edges are (source, destination, weight):\n";
    int sum = 0;
    for (int i = 0; i < noVertex - 1; i++)
    {
        sum += edges[i].weight;
        cout << treeEdges[i].source << " " << edges[i].dest << " " << edges[i].weight << "\n";
    }
    cout << "Cost : " << sum << "\n";
}

int getRoot(int vertex)
{
    if (parent[vertex] == vertex)
    {
        return vertex;
    }
    return getRoot(parent[vertex]);
}

void sort()
{

    for (int i = 0; i < noEdges - 1; ++i)
    {
        auto min = i;
        for (int j = i + 1; j < noEdges; ++j)
        {
            if (edges[j].weight < edges[min].weight)
            {
                min = j;
            }
        }

        auto temp = edges[min];
        edges[min] = edges[i];
        edges[i] = temp;
    }
}

void kruskal()
{

    parent = new int[noVertex];
    for (int i = 0; i < noVertex; i++)
    {
        parent[i] = i;
    }

    sort();

    treeEdges = new Edge[noVertex - 1];

    int count = 0, i = 0;
    while (count < noVertex - 1)
    {
        int sourceP = getRoot(edges[i].source);
        int destP = getRoot(edges[i].dest);

        if (sourceP != destP)
        {
            treeEdges[count++] = edges[i];
            parent[sourceP] = destP;
        }
        ++i;
    }

    free(parent);
}

int main()
{

    readGraph();

    Profiler *profiler = new Profiler();
    profiler->start();

    kruskal();

    profiler->end();

    printGraph();

    cout << "\nProfiler Information\n";
    profiler->printStats();

    free(profiler);
    free(edges);
    free(treeEdges);

    return 0;
}


// Enter The number of edges : 10  
// Enter The number of vertices : 7
// Enter the vertices : (source, destination, weight)
// 0 1 6
// 0 1 9
// 1 7 5
// 1 3 2
// 7 3 2
// 3 2 3
// 1 2 4
// 2 0 3
// 2 6 8
// 6 0 7
