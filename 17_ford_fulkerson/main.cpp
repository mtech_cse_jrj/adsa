#include <iostream>
#include <limits.h>
#include <string.h>
#include <queue>
#include <fstream>
#include <sstream>

using namespace std;

#define MAX_SIZE 60

bool bfs(int graph[MAX_SIZE][MAX_SIZE], int s, int t, int parent[])
{
    bool visited[MAX_SIZE];
    memset(visited, 0, sizeof(visited));

    queue<int> q;
    q.push(s);
    visited[s] = true;
    parent[s] = -1;

    while (!q.empty())
    {
        int u = q.front();
        q.pop();

        for (int v = 0; v < MAX_SIZE; v++)
        {
            if (visited[v] == false && graph[u][v] > 0)
            {
                q.push(v);
                parent[v] = u;
                visited[v] = true;
            }
        }
    }

    return (visited[t] == true);
}

int ford_fulkerson(int graph[MAX_SIZE][MAX_SIZE], int s, int t)
{
    int u, v;

    int m_graph[MAX_SIZE][MAX_SIZE];

    for (u = 0; u < MAX_SIZE; u++)
        for (v = 0; v < MAX_SIZE; v++)
            m_graph[u][v] = graph[u][v];

    int parent[MAX_SIZE];

    int max_flow = 0;

    while (bfs(m_graph, s, t, parent))
    {
        int path = INT_MAX;
        for (v = t; v != s; v = parent[v])
        {
            u = parent[v];
            path = min(path, m_graph[u][v]);
        }

        for (v = t; v != s; v = parent[v])
        {
            u = parent[v];
            m_graph[u][v] -= path;
            m_graph[v][u] += path;
        }

        max_flow += path;
    }

    return max_flow;
}

int main(int argc, char **argv)
{

    ifstream file(argv[1]);

    int graph[MAX_SIZE][MAX_SIZE];

    cout << "Reading Flow Network\n";

    string line;
    int row;
    while (getline(file, line))
    {
        std::istringstream text_stream(line);
        cout << line << "\n";
        text_stream >> graph[row][0] >> graph[row][1] >> graph[row][2] >> graph[row][3] >> graph[row][4] >> graph[row][5];
        row++;
    }

    int max_flow = ford_fulkerson(graph, 0, 5);

    cout << "The maximum flow is " << max_flow << "\n";

    return 0;
}